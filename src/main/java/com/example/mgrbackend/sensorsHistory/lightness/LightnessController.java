package com.example.mgrbackend.sensorsHistory.lightness;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;

@Controller
public class LightnessController {
    LightnessService lightnessService;

    LightnessController(LightnessService lightnessService) {
        this.lightnessService = lightnessService;
    }

    @ResponseBody
    @PostMapping(value = "/add_lightness")
    public HashMap<String, String> registerTemperature(@RequestBody Lightness lightness) {
        lightnessService.addLightness(lightness);
        System.out.println("Registered".concat(lightness.toString()));
        HashMap<String, String> response = new HashMap<>();
        response.put("result", "added");
        return response;
    }

    @ResponseBody
    @GetMapping( value = "/lightness_history")
    public List<Lightness> getLightness() {
        return lightnessService.getAllLightness();
    }
}
