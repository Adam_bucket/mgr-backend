package com.example.mgrbackend.sensorsHistory.lightness;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface LightnessRepository extends JpaRepository<Lightness, Long> {
    @Query("SELECT s FROM Lightness s WHERE s.value = ?1")
    Optional<Lightness> findLightness(String value);
}
