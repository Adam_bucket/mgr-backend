package com.example.mgrbackend.sensorsHistory.lightness;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;

@Service
public class LightnessService {
    LightnessRepository lightnessRepository;
    boolean pragmaOnce = false;
    double initalTime;

    @Autowired
    LightnessService(LightnessRepository lightnessRepository) {
        this.lightnessRepository = lightnessRepository;
    }

    public List<Lightness> getAllLightness() {
        return lightnessRepository.findAll();
    }

    public void addLightness(Lightness lightness) {
        try {
            lightnessRepository.save(lightness);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addLightness(String value){
        Date date = new Date();
        Double time = (double) date.getTime();
        if(!pragmaOnce) {
            initalTime = time;
            pragmaOnce = true;
        }

        Lightness lightness = new Lightness(value, date, (time - initalTime)/1000);
        try {
            lightnessRepository.save(lightness);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    };
}
