package com.example.mgrbackend.sensorsHistory.temperature;

import com.example.mgrbackend.sensor.Sensor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Service
public class TemperatureService {
    TemperatureRepository temperatureRepository;
    boolean pragmaOnce = false;
    double initalTime;

    @Autowired
    TemperatureService(TemperatureRepository temperatureRepository) {
        this.temperatureRepository = temperatureRepository;
    }

    public List<Temperature> getAllTemperatures() {
        return temperatureRepository.findAll();
    }

    public void addTemperature(Temperature temperature) {
        try {
            temperatureRepository.save(temperature);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void addTemperature(String value){
        Date date = new Date();
        Double time = (double) date.getTime();
        if(!pragmaOnce) {
            initalTime = time;
            pragmaOnce = true;
        }

        Temperature temperature = new Temperature(value, date, (time - initalTime)/1000);
        try {
            temperatureRepository.save(temperature);
        }
        catch (Exception e) {
            System.out.println(e.getMessage());
        }
    };
}
